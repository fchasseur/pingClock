/*-------------------------------------------------------------------------
  This file is part of the CLOCKPASS Project.
  
  Written by Francois Chasseur / TRAKK.be 
            
  You should have received a copy of the GNU Lesser General Public
  License along.  If not, see  <http://www.gnu.org/licenses/>.
  -------------------------------------------------------------------------*/

byte  chiffres[] [10] ={
              {0xf8,0xf8,0xd8,0xd8,0xd8,0xd8,0xd8,0xd8,0xf8,0xf8}, //0
              {0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18}, //1
              {0xf8,0xf8,0x18,0x18,0xf8,0xf8,0xc0,0xc0,0xf8,0xf8}, //2
              {0xf8,0xf8,0x18,0x18,0xf8,0xf8,0x18,0x18,0xf8,0xf8}, //3
              {0xd8,0xd8,0xd8,0xd8,0xf8,0xf8,0x18,0x18,0x18,0x18}, //4
              {0xf8,0xf8,0xc0,0xc0,0xf8,0xf8,0x18,0x18,0xf8,0xf8}, //5
              {0xf8,0xf8,0xc0,0xc0,0xf8,0xf8,0xd8,0xd8,0xf8,0xf8}, //6
              {0xf8,0xf8,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18}, //7
              {0xf8,0xf8,0xd8,0xd8,0xf8,0xf8,0xd8,0xd8,0xf8,0xf8}, //8
              {0xf8,0xf8,0xd8,0xd8,0xf8,0xf8,0x18,0x18,0xf8,0xf8}  //9
};
// Inclure ADAFruit_GFX and RTCLib ! 

#include <Adafruit_GFX.h>
#include "ws2801Matrix.h"

#include <Wire.h>
#include "RTClib.h"

int posText = 0 ;
#include <math.h>   

#define BTN_H 3
#define BTN_M 4




RTC_DS1307 rtc;

ws2801Matrix* matrix ;
void setup() {
  pinMode(A3, OUTPUT);
  pinMode(A2, OUTPUT);


  pinMode(BTN_H, INPUT_PULLUP);
  pinMode(BTN_M, INPUT_PULLUP);

  
  digitalWrite(A3, HIGH);
  digitalWrite(A2, LOW);

  Serial.begin(9600);
  Serial.println("Setup");

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
   
  }



  matrix = new ws2801Matrix(12, 10, 5, 6,
                            NEO_MATRIX_BOTTOM     + NEO_MATRIX_LEFT +
                            NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG);

  matrix->begin();
  matrix->setTextWrap(false);

  matrix->setTextColor(matrix->Color(255, 255, 255));
  posText = 12;
  Serial.println("SetupDone");
}

int sec = 0;
long timeOutPixels = 0;
long timeOutText = 0;

int x = 0;
int y = 0;
int temp = 0;
float count = 0 ;

void display(int id,int pos,byte fadeIntensity)
{
   for( int i = 0 ; i< 12; i ++)
   {
    byte line = chiffres[id][i];
    for(int j= 0 ; j< 5 ; j ++)
    {
      if( line & (1 << 3+j))
      {
        matrix->drawPixel( pos + 4-j,i, matrix->Color(fadeIntensity,fadeIntensity,fadeIntensity));
      }
      else
      {
       // matrix->drawPixel(pos + 4-j,i, matrix->Color(128,0,0));   
      }
      
    }
   }
  
}


byte intensity = 0 ;
long timeOutFade = 0 ;
byte step = 10;

#define   HOURS 0 
#define DOTS 1 
#define  MINUTES 2 
long chrono = 0 ;
byte state  = HOURS;
void displayTime()
{
    DateTime now = rtc.now();
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
}
void loop() {

   DateTime now = rtc.now();
   
    //for(int i = 0; i<10; i ++)
    int min10 = now.minute()/10;
    int min1 = now.minute()%10;
    int h1 = now.hour()%10;
    int h10 = now.hour()/10;

    
  if( digitalRead(BTN_M) == LOW  || digitalRead(BTN_H) == LOW )
  { 
    delay(50);
    int count = 0 ;

    if( digitalRead(BTN_M) == LOW && digitalRead(BTN_H) == HIGH)
    {
     
             Serial.println("Adding Minutes");
          matrix->fillScreen(matrix->Color(0,0,255));
          rtc.adjust(DateTime(2017, 3, 15,
                      h10*10 + h1 , 
                      (min10*10+min1 +1)%60, 0));
          min1 = now.minute()%10;
          min10 = now.minute()/10;

          display(min10, 0, 255);
          display(min1, 7, 255);
       
          matrix->show();
          delay(100);
          timeOutFade = millis() + 4000;
         
    }
    if( digitalRead(BTN_H) == LOW && digitalRead(BTN_M) == HIGH)
    {
          Serial.println("Adding Hour");
          matrix->fillScreen(matrix->Color(0,255,0));
          rtc.adjust(DateTime(2017, 3, 15,
                      h10*10 + h1 + 1, 
                      min10*10+min1, 0));
          h1 = now.hour()%10;
          h10 = now.hour()/10;

          display(h10, 0, 255);
          display(h1, 7, 255);
       
          matrix->show();
          delay(100);
          timeOutFade = millis() + 4000;
    }
    
    while( digitalRead(BTN_M) == LOW  && digitalRead(BTN_H) == LOW )
    {
       count +=1;
       delay(100);
    }
    if( count > 50)
    {
       matrix->fillScreen(matrix->Color(255,0,0));
       display(0, 0, 255);
       display(0, 7, 255);
       matrix->show();
       rtc.adjust(DateTime(2017, 3, 15, 0, 0, 0));
       displayTime();
       timeOutFade = millis() + 4000;
    }

    
    
    
  }
  if( millis() > timeOutFade)
  {
   

     matrix->fillScreen(matrix->Color(0,0,0));
     
    
   

    
    switch(state)
    {
      case HOURS:
       display(h10, 0, intensity);
       display(h1, 7, intensity);
      break;
      case DOTS:
       matrix->fillRect(5,3,2,2, matrix->Color(intensity,intensity,intensity));
       matrix->fillRect(5,6,2,2, matrix->Color(intensity,intensity,intensity));
       break;
      case MINUTES:
       display(min10, 0, intensity);
       display(min1, 7, intensity);
       break;
    }
     matrix->show();
     timeOutFade = millis() + 10;

      intensity += step;
    
    if( intensity >= 250)
    {
      timeOutFade = millis() + 4000;
      displayTime();
      step = -5 ;
    }
    if( intensity <=0)
    {
      step = 10;
      state = (state+1)%3;
    }
  }

  
}
  

